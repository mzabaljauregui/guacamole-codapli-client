import { Component, OnInit } from '@angular/core';

import { RemoteDesktopManager } from '@illgrenoble/ngx-remote-desktop';
import { WebSocketTunnel } from '@illgrenoble/guacamole-common-js';
import  *  as  crypto  from 'crypto';

@Component({
    selector: 'app-root',
    template:`
    <div class="vdi-container">
    <ngx-remote-desktop [manager]="manager">
      <!-- Toolbar items -->
      <ngx-remote-desktop-toolbar-item *ngIf="manager.isConnected()" (click)="handleDisconnect()" align="right">
        <i class="fa fa-sign-out"> </i> Disconnect
      </ngx-remote-desktop-toolbar-item>
      <ngx-remote-desktop-toolbar-item (click)="handleEnterFullScreen()" *ngIf="!manager.isFullScreen() && manager.isConnected()" align="right">
          <i class="fa fa-arrows-alt" aria-hidden="true"></i> Enter full screen
      </ngx-remote-desktop-toolbar-item>
      <ngx-remote-desktop-toolbar-item (click)="handleExitFullScreen()" *ngIf="manager.isFullScreen() && manager.isConnected()" align="right">
          Exit full screen
      </ngx-remote-desktop-toolbar-item>
  
      <!-- Override connection state messages -->
      <ngx-remote-desktop-connecting-message>
        <div class="ngx-remote-desktop-message-title ngx-remote-desktop-message-title-success">
          CONNECTING TO REMOTE DESKTOP
        </div>
        <div class="ngx-remote-desktop-message-body">
          Attempting to connect to the remote desktop. Waiting for response...
        </div>
      </ngx-remote-desktop-connecting-message>
  
      <!-- Status bar -->
      <ngx-remote-desktop-status-bar *ngIf="manager.isConnected()">
        <ngx-remote-desktop-status-bar-item>
            You are currently connected to: <strong>Machine 1</strong>
        </ngx-remote-desktop-status-bar-item>
        <ngx-remote-desktop-status-bar-item>
            <span>Need help? Look at our <a href="#">documentation</a></span>
        </ngx-remote-desktop-status-bar-item>
      </ngx-remote-desktop-status-bar>
      
    </ngx-remote-desktop>
  </div>  
    `
})
export class AppComponent implements OnInit {

    manager: RemoteDesktopManager;
    client:any;
    clientOptions:any = {
        cypher: 'AES-256-CBC',
        key: 'MySuperSecretKeyForParamsToken12'
    }

    constructor(){
    }

    handleHelp() {
        console.log('Hello help');
    }

    handleDisconnect(): void {
        this.manager.getClient().disconnect();
    }

    handleEnterFullScreen() {
        this.manager.setFullScreen(true);
    }

    handleExitFullScreen() {
        this.manager.setFullScreen(false);
    }

    ngOnInit() {
        // Setup tunnel. The tunnel can be either: WebsocketTunnel, HTTPTunnel or ChainedTunnel
        const tunnel = new WebSocketTunnel('ws://localhost:5200');
        /**
         *  Create an instance of the remote desktop manager by 
         *  passing in the tunnel and parameters
         */
        this.manager = new RemoteDesktopManager(tunnel);

              // URL parameters (image, audio and other query parameters you want to send to the tunnel.)
        const parameters = {
            type:"vnc",
            settings: {
                "hostname": "192.168.88.30",
                "username": "usuario",
                "password": "usuario",
                
            }
        };
        /*
         * The manager will establish a connection to: 
         * ws://localhost:8080?width=n&height=n&ip=192.168.13.232&image=image/png
         */
        const encriptParams = this.encript( {connection: parameters});
        console.log(encriptParams);
        this.manager.connect({token: encriptParams});
    }

    encript(value) {
        const iv = crypto.randomBytes(16);
        const cipher = crypto.createCipheriv(this.clientOptions.cypher, this.clientOptions.key, iv);
     
        let crypted = cipher.update(JSON.stringify(value), 'utf8', 'base64');
        crypted += cipher.final('base64');
     
        const data = {
            iv: iv.toString('base64'),
            value: crypted
        };
     
        return new Buffer(JSON.stringify(data)).toString('base64');
    
    }
}