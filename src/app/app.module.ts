import { BrowserModule } from '@angular/platform-browser';
import { NgxRemoteDesktopModule } from '@illgrenoble/ngx-remote-desktop';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxRemoteDesktopModule,
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
